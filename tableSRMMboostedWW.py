#! /usr/bin/python
#

import posix
import getopt, string
import posix
import time
from stat import *
import os.path

f1=open("C1C1_WW_400_0_MM.txt","r")
f2=open("C1C1_WW_300_100_MM.txt","r")
fout=open("hepData_SRMMboostedWW.yaml","w")

f11=open("dataSRMMboostedWW.txt","r")
f22=open("mcSRMMboostedWW.txt","r")
f33=open("Bins.txt","r")




#C1C1_600_0
fout.writelines("dependent_variables:"+"\n")
fout.writelines("- header: {name: 'm($\tilde{\chi^\pm_1}$, $\tilde{\chi^0_1}$)=(300,100) GeV', units: ''}"+"\n")
fout.writelines("  qualifiers: "+"\n")
fout.writelines("  - {name: SQRT(S), units: GEV, value: '13000.0'}"+"\n")
fout.writelines("  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: '139'}"+"\n")
fout.writelines("  values:"+"\n")

lines1 = f2.readlines()
#print (lines1)
for line1 in lines1:
    if not "Double" in line1 and not "};" in line1:        
      point1=line1.replace("\n","")
      #print (point1)           
      point1=point1.replace(",","")
      #print (point1)
      fout.writelines('  - errors: []' +"\n")
      fout.writelines('    value: ')
      fout.writelines("'"+("%.3f" % float(point1))+"'"+"\n")
      
      
      
#fout.writelines("independent_variables:"+"\n")
#fout.writelines("- header: {name: M(GLUINO), units: GEV}"+"\n")
#fout.writelines("  values:"+"\n")

#C1C1_400_0
 
fout.writelines("- header: {name: 'm($\tilde{\chi^\pm_1}$, $\tilde{\chi^0_1}$)=(400,0) GeV', units: ''}"+"\n")
fout.writelines("  qualifiers: "+"\n")
fout.writelines("  - {name: SQRT(S), units: GEV, value: '13000.0'}"+"\n")
fout.writelines("  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: '139'}"+"\n")
fout.writelines("  values:"+"\n")

lines2 = f1.readlines()
#print (lines1)
for line2 in lines2:
    if not "Double" in line2 and not "};" in line2:        
      point2=line2.replace("\n","")
      #print (point2)           
      point2=point2.replace(",","")
      #print (point2)
      fout.writelines('  - errors: []' +"\n")
      fout.writelines('    value: ')
      fout.writelines("'"+("%.3f" % float(point2))+"'"+"\n")
      


#data
fout.writelines("- header: {name: 'Data [Events/Bin]'}"+"\n")
fout.writelines("  qualifiers: "+"\n")
fout.writelines("  - {name: SQRT(S), units: GEV, value: '13000.0'}"+"\n")
fout.writelines("  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: '139'}"+"\n")
fout.writelines("  values:"+"\n")

lines3 = f11.readlines()
#print (lines3)
for line3 in lines3:
    if not "Double" in line3 and not "};" in line3:
     if "y=" in line3:
      point3=line3.replace("\n","")
      #print (point3)           
      point3=point3.replace("y=","")
      point3=point3.replace(",","")
      #print ("aici",point3)
     if "yErrlow=" in line3:
      ErrLow=line3.replace("\n","")
      #print (ErrLow)           
      ErrLow=ErrLow.replace("yErrlow=","")
      ErrLow=ErrLow.replace(",","")
      #print ("aici",ErrLow)
     if "yErrhigh=" in line3:
      ErrHigh=line3.replace("\n","")
      #print (ErrHigh)           
      ErrHigh=ErrHigh.replace("yErrhigh=","")
      ErrHigh=ErrHigh.replace(",","")
      #print ("aici",ErrHigh)  
      fout.writelines('  - errors: ' +"\n")
      fout.writelines('    - asymerror: {minus:'+(" %.3f" % float(ErrLow))+", plus: "+(" %.3f" % float(ErrHigh))+"}" +"\n")
      fout.writelines('    value: ')
      fout.writelines("'"+("%.3f" % float(point3))+"'"+"\n")

#mc
fout.writelines("- header: {name: 'Expected Background [Events/Bin]'}"+"\n")
fout.writelines("  qualifiers: "+"\n")
fout.writelines("  - {name: SQRT(S), units: GEV, value: '13000.0'}"+"\n")
fout.writelines("  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: '139'}"+"\n")
fout.writelines("  values:"+"\n")

lines4 = f22.readlines()
#print (lines4)
for line4 in lines4:
    if not "Double" in line4 and not "};" in line4:
     if "y=" in line4:
      point4=line4.replace("\n","")
      #print (point4)           
      point4=point4.replace("y=","")
      point4=point4.replace(",","")
      #print ("aici",point4)
     if "yErrlow=" in line4:
      ErrLow=line4.replace("\n","")
      #print (ErrLow)           
      ErrLow=ErrLow.replace("yErrlow=","")
      ErrLow=ErrLow.replace(",","")
      #print ("aici",ErrLow)
     if "yErrhigh=" in line4:
      ErrHigh=line4.replace("\n","")
      #print (ErrHigh)           
      ErrHigh=ErrHigh.replace("yErrhigh=","")
      ErrHigh=ErrHigh.replace(",","")
      #print ("aici",ErrHigh)  
      fout.writelines('  - errors: ' +"\n")
      fout.writelines('    - asymerror: {minus:'+(" %.3f" % float(ErrLow))+", plus: "+(" %.3f" % float(ErrHigh))+"}" +"\n")
      fout.writelines('      label: sys $\otimes$ stat' +"\n")
      fout.writelines('    value: ')
      fout.writelines("'"+("%.3f" % float(point4))+"'"+"\n")
      

#bins
fout.writelines("independent_variables:"+"\n")
fout.writelines("- header: {name: '$m_{eff}$ [GeV]'}"+"\n")
fout.writelines("  values:"+"\n")

lines5 = f33.readlines()
print (lines5)

for line5 in lines5:
    if not "Int" in line5 and not "};" in line5:
     if "xBinLowEdge" in line5:
      xBinLowEdge=line5.replace("\n","")
      #print (xBinLowEdge)           
      xBinLowEdge=xBinLowEdge.replace("xBinLowEdge","")
      xBinLowEdge=xBinLowEdge.replace(",","")
      xBinLowEdge=xBinLowEdge.replace("[1]=","")
      xBinLowEdge=xBinLowEdge.replace("[2]=","")
      xBinLowEdge=xBinLowEdge.replace("[3]=","")
      xBinLowEdge=xBinLowEdge.replace("[4]=","")
      
      #print ("aici",xBinLowEdge)
     if "xBinUpEdge" in line5:
      xBinUpEdge=line5.replace("\n","")
      #print (xBinUpEdge)           
      xBinUpEdge=xBinUpEdge.replace("xBinUpEdge","")
      
      xline=xBinUpEdge.replace(","," ")
      #xline=xBinUpEdge.replace("="," ")
      
      xBinUpEdge=xBinUpEdge.replace(",","")
      xBinUpEdge=xBinUpEdge.replace("[1]=","")
      xBinUpEdge=xBinUpEdge.replace("[2]=","")
      xBinUpEdge=xBinUpEdge.replace("[3]=","")
      xBinUpEdge=xBinUpEdge.replace("[4]=","")
      if "=" in xline:
        xline1=xline.replace("=850","")
        xline1=xline1.replace("=1100","")
        xline1=xline1.replace("=1350","")
        xline1=xline1.replace("=1600","")
        print ("aici",xline1)
      
        fout.writelines('  - {'" low: "+(xBinLowEdge)+", "+" high: "+(xBinUpEdge)+"}" +"\n")
      	


print ("wrote ", fout.name)

#print "wrote ", fout1.name


f1.close()
fout.close()

##f11.close()
#fout1.close()

